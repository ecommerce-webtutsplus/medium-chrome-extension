const loggly = new LogglyTracker();
loggly.push({
  logglyKey: 'c5cb1f4e-0af5-459d-8e74-dd390ae4215d',
  sendConsoleErrors: true,
  tag: 'mes-stats'
});

log('neel start');

const { from, fromEvent, merge, combineLatest, timer, BehaviorSubject, Subject } = rxjs;
const { tap, map, mapTo, exhaustMap, filter, debounceTime, delay } = rxjs.operators;

const BAR_CHART_DATE_IDS = generateDateIds();
let barChartIdsOffest = 0;
let barChartExtrasDOM;
const stateSubject = new BehaviorSubject(undefined);
const state$ = stateSubject.asObservable().pipe(
  filter(s => !!s),
  map(s => urlIncludes('responses') ? s.user.totals.responses : s.user.totals.articles)
);

state$.subscribe(s => {
  log('new state');
});

// periodically check for new page
timer(0, 1000000000)
  .pipe(
    filter(isNewPage),
    tap(cleanBarChartExtras),
    exhaustMap(() => from(loadData()))
  )
  .subscribe(data => {
    console.log("data neel", data);
  });


function loadData() {
  log('load data');
  return new Promise((resolve) =>
    chrome.runtime.sendMessage({ type: 'GET_TOTALS'}, {}, data => resolve(data)));
}

function loadPostStats(postId) {
  log('load post stats');
  return new Promise((resolve) =>
    chrome.runtime.sendMessage({ type: 'GET_POST_STATS', postId }, {}, data => resolve(data)));
}

function cleanBarChartExtras() {
  if (barChartExtrasDOM) {
    barChartExtrasDOM.innerHTML = '';
  }
  cleanBarChartPostBars();
}

function cleanBarChartPostBars() {
  document.querySelectorAll('.mes-post-bar').forEach(node => node.remove());
}


function isNewPage() {
  return urlIncludes('stats') && !document.querySelector('table tfoot');
}

function urlIncludes(text) {
  return window.location.pathname.includes(text);
}

function formatValue(number = 0) {
  return number >= 1000000000
    ? (Math.floor(number / 100000000) / 10).toFixed(1) + 'B'
    : number >= 1000000
      ? (Math.floor(number / 100000) / 10).toFixed(1) + 'M'
      :  number >= 1000
        ? (Math.floor(number / 100) / 10).toFixed(1) + 'K'
        : number.toFixed(0);
}

function formatWholeNumber(number = 0) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getDateIds() {
  const offset = barChartIdsOffest === 0 ? 0 : barChartIdsOffest * 30;
  return BAR_CHART_DATE_IDS.slice(offset, offset + 30);
}

function generateDateIds() {
  const endDate = new Date();
  let startDate = new Date(endDate.getFullYear() - 10, endDate.getMonth(), endDate.getDate());
  let start = startDate.getTime();
  const end = endDate.getTime();
  const oneDay = 24 * 3600 * 1000;
  const ids = [];
  for (; start < end; start += oneDay) {
    startDate = new Date(start);
    ids.push(`${startDate.getFullYear()}-${startDate.getMonth()}-${startDate.getDate()}`);
  }
  return ids.reverse();
}

function log(...args) {
  console.log('Medium Enhanced Stats neel [stats] -', ...args);
}
