const loggly = new LogglyTracker();
loggly.push({
  logglyKey: 'c5cb1f4e-0af5-459d-8e74-dd390ae4215d',
  sendConsoleErrors: true,
  tag: 'mes-background',
});

const perf = new LogglyTracker();
perf.push({
  logglyKey: 'c5cb1f4e-0af5-459d-8e74-dd390ae4215d',
  tag: 'mes-perf',
});

const API_URL = 'https://medium.com';
const timers = {};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  const { type, postId } = request;
  if (type === 'GET_TOTALS') {
    handleGetTotals().then(sendResponse);
  }
  return true; // enable async sendResponse
});

chrome.runtime.onInstalled.addListener((details) => {
  if (['install', 'update'].includes(details.reason)) {
    const feedbackFormId =
      '1FAIpQLSdItN10f-8zD6amnFu-WfjTB8rq_ACeHW3r-WRse0N620-UNQ';
    const feedbackFormUrl = `https://docs.google.com/forms/d/e/${feedbackFormId}/viewform`;
    if (chrome.runtime.setUninstallURL) {
      chrome.runtime.setUninstallURL(feedbackFormUrl);
    }
  }
});

function getTotals(url, payload) {
  let finalUrl = `${API_URL}${url}?limit=1000`;
  logBackground(finalUrl);
  if (!payload) {
    return request(finalUrl).then((res) => getTotals(url, res));
  }
  const { value, paging } = payload;
  if (
    payload &&
    paging &&
    paging.next &&
    paging.next.to &&
    value &&
    value.length
  ) {
    finalUrl += `&to=${paging.next.to}`;
    return request(finalUrl).then((res) => {
      payload.value = [...payload.value, ...res.value];
      payload.paging = res.paging;
      return getTotals(url, payload);
    });
  } else {
    console.log("neel payload", payload);
    return Promise.resolve(payload);
  }
}

function handleGetTotals() {
  timer('user');
  return Promise.all([getTotals('/me/stats')])
    .then(([articles]) => {
      perf.push({ time: timer('user'), type: 'request-user' });
      return articles.value;
    }).then((articles) => {
      console.log("ARTICLES", articles);
      let promArr = [];
      let i = 0;
      articles.forEach((article) => {
        //promArr.push(addToDatabase('http://0.0.0.0:5000/add-articles', article));
        logBackground(i , article);
        i += 1;
      });
      Promise.all(promArr).then((res) => {
        return [articles];
      });
      });
}

function request(url) {
  return fetch(url, {
    credentials: 'same-origin',
    headers: {
      accept: 'application/json',
    },
  })
    .then((res) => res.text())
    .then((text) => JSON.parse(text.slice(16)).payload);
}


function addToDatabase(url, post) {
  console.log("post neel", post);
  body = {
    //credentials: 'same-origin',
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({url: getUrl(post)})
  };
  console.log("neel body", body);
    return fetch(url, body)
      .then((res) =>
          res.json())
}

function getUrl(post) {
  return "https://medium.com/nilmadhab/" + post.slug + "-" + post.postId
}



function timer(id) {
  if (!timers[id]) {
    timers[id] = window.performance.now();
  } else {
    const result = window.performance.now() - timers[id];
    delete timers[id];
    return result;
  }
}

function logBackground(...args) {
  console.log('Medium Enhanced Stats [background] -', ...args);
}
